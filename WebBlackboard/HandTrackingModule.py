import cv2
import mediapipe as mp
import time

#HOW TO USE MEDIAPIPE:

#MediaPipe is a Google library used to detect body parts (not only hands, hand is the use case for this project)
#It can detect landmarks with certain confidence, for 1 or 2 hands (those values can be set according to your needs)
#It works on RGB image, detecting approx. 20 landmarks for hand, with predefined indexes (see google docs for index values)
#Landmarks and landmark links can be drawn using draw_landmark
#Indexes are returned as list of 3D normalized coordonates (to be denormalized, muktiply them by image width/height/depth)



#Class to find hand properties using mediapipe
class HandDetector():
    
    def __init__(self, mode = False, maxHands = 2, modelComplexity = 1,detectionConf = 0.5, trackConf = 0.5):

        self.mpHand = mp.solutions.hands
        self.hands = self.mpHand.Hands(mode, maxHands, modelComplexity,detectionConf, trackConf) #detect hands
        self.mpDraw = mp.solutions.drawing_utils

    #Find and draw hand landmarks
    def findHands(self, image, draw = True):
        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        self.results = self.hands.process(image_rgb)

        if self.results.multi_hand_landmarks:
            for handLM in self.results.multi_hand_landmarks:
               if draw:
                    self.mpDraw.draw_landmarks(image, handLM, self.mpHand.HAND_CONNECTIONS) #draw landmarks and links
        return image

    #Get x and y coordonates of hand landmark
    def getHandPoints(self, image, handNo = 0):
        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        h, w, c = image.shape
        self.handPoints = []
        if self.results.multi_hand_landmarks:
            hand = self.results.multi_hand_landmarks[handNo]
            for id, landmark in enumerate(hand.landmark):
                p_x, p_y = int(landmark.x * w), int(landmark.y * h)
                self.handPoints.append([id, p_x, p_y])
        return self.handPoints

    #Get what fingers are up (1) and are down (0)
    def fingersUp(self):

        fingersId = [4, 8, 12, 16, 20]
        fingers = []

        isRight = 0
        if self.handPoints[fingersId[0]][1] < self.handPoints[fingersId[1]][1]:
            isRight = 1
        if self.handPoints[fingersId[0]][1] < self.handPoints[fingersId[0] - 1][1]: #Thumb finger is up
            fingers.append(isRight)
        else:
            fingers.append(1 - isRight)
            
        for i in range(1, 5):
            if self.handPoints[fingersId[i]][2] < self.handPoints[fingersId[i] - 2][2]: #Finger is up
                fingers.append(1)
            else:
                fingers.append(0)

        return fingers

if __name__ == '__main__':
    pTime = 0
    cTime = 0

    cap = cv2.VideoCapture(0)
    detector = HandDetector()
    while cap.isOpened():
        success, image = cap.read()

        if success:
            image = detector.findHands(image)
            hand_list = detector.getHandPoints(image)

            print(hand_list)
            cTime = time.time()
            fps = 1 / (cTime - pTime)
            pTime = cTime

            cv2.putText(image, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_COMPLEX, 3, (255, 0, 255), 3, )
            cv2.imshow("Original image", image)
        cv2.waitKey(1)
