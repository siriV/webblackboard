import cv2
import numpy as np
import time
import os
import HandTrackingModule as htm

if __name__ == '__main__':
    folder_path = "header"
    headers_path = os.listdir(folder_path)
    headers = []
    #load drawn utilities' menu
    for header_path in headers_path:
        head = cv2.imread(f"{folder_path}/{header_path}")
        headers.append(head)

    header = headers[0]
    header_h = 125
    header_w = 1280
    brushThickness = 15
    eraserThickness = 50
    thickness = brushThickness
    cap = cv2.VideoCapture(0)
    cap.set(3 ,1280)
    cap.set(4, 720)
    detector = htm.HandDetector(detectionConf = 0.75, maxHands=1) #get one hand
    drawColor = (0, 255, 0)
    canvas = np.zeros((720, 1280, 3), np.uint8)
    if_xp, if_yp = -1, -1
    while cap.isOpened():
        success, img = cap.read()
        img = cv2.flip(img, 1) #flip image to match hand movement
        if success:

            img = detector.findHands(img)
            handLM = detector.getHandPoints(img)
            #if hand is detected
            if len(handLM) != 0:

                if_x, if_y = handLM[8][1:]
                mf_x, mf_y = handLM[12][1:]

                fingers = detector.fingersUp()
                img[0:125, 0:1280] = header
                if fingers[1] and fingers[2]: #if 2 fingers are up
                    cv2.rectangle(img, (if_x, if_y - 40), (mf_x, mf_y + 40), drawColor, cv2.FILLED)
                    if if_y < header_h: # if inside selection pannel
                        if 230 < if_x < 310:
                            drawColor = (0, 255, 0)
                            header = headers[0]
                        elif 500 < if_x < 600:
                            drawColor = (255, 0, 0)
                            header = headers[1]
                        elif 850 < if_x < 930:
                            drawColor = (0, 0, 255)
                            header = headers[2]
                        elif 1070 < if_x < 1180:
                            drawColor = (0, 0, 0)
                            header = headers[3]
                    if_xp, if_yp = -1, -1
                elif fingers[1] and if_y > 125: # if one finger is uo and insede drawing pannel
                    cv2.circle(img, (if_x, if_y), 20, drawColor, cv2.FILLED)
                    if if_xp != -1 and if_yp != -1:
                        if drawColor == (0, 0 , 0):
                            thickness = eraserThickness
                        else:
                            thickness = brushThickness
                        cv2.line(canvas, (if_xp, if_yp), (if_x, if_y), drawColor, thickness)

                    if_xp, if_yp = if_x, if_y
            else:
                img[0:125, 0:1280] = header
                if_xp, if_yp = -1, -1

            #draw with color and display on canvas
            gray_img = cv2.cvtColor(canvas, cv2.COLOR_BGR2GRAY)
            _, thresh_img = cv2.threshold(gray_img, 20, 255, cv2.THRESH_BINARY_INV)
            thresh_img = cv2.cvtColor(thresh_img, cv2.COLOR_GRAY2BGR)
            img = cv2.bitwise_and(img, thresh_img)
            img = cv2.bitwise_or(img, canvas)

            cv2.imshow("Result", img)
            cv2.imshow("Canvas", canvas)
        cv2.waitKey(1)
